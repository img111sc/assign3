// Chris Stevens-Edgar, assignment 3, IMG111, Feb-5-18
//Using the class definition to create Product (available in data.js) ,
//create 6 products with properties as shown in the solution: product number,
//description, available quantity, price and fee.
//class object products

class Product{
constructor( productNumber, description, availQuantity, price, fee){
this.productNumber = productNumber;
this.description = description;
this.availQuantity = availQuantity;
this.price = price;
this.fee = fee;
}
totalAmt() {
    return this.price + this.fee;
  }


} 
//create variables
var product1 = new Product("754", "this product1", "5700", 20.00, 1.50);
var product2 = new Product("839", "this product2", "2000", 54.00, 0.56);
var product3 = new Product("912", "this product3", "350", 14.00, 0.26);
var product4 = new Product("1120", "this product4", "4224", 2.00, 1.00);
var product5 = new Product("1230", "this pproduct5", "7", 9.99, 0.00);
var product6 = new Product("1023","this product6" , "1", 5000.00, 0.76);

//Place the above created objects into an array called products.
var products = []
products.push(product1);                     
products.push(product2);
products.push(product3);
products.push(product4);
products.push(product5);
products.push(product6);

//Write a function getShuffledUniqueImageNames(myArrayOfImages) that returns an
//array of random image names without duplicates. Remember, that all image names
//are available in the array pictures. Be sure that the original pictures array is not changed.
function getShuffledUniqueImageNames(value1){
//create a varible =   =myArrayOfImages .slice check w3
var copyArray =value1.slice (0);
// creating for loop
//steps
//set index to zero
// index<? if index is less  then ur (end)< usualy variable.length
//always (++1) to index (++1)
//tells what to do
// for loop
for (var index = 0; index <copyArray.length; index++) {
//generate random image number
//always use Math.floor Math.random
var randIndex = Math.floor((Math.random() * copyArray.length));
//create an array  [COPY] THE ARRAY (data.js) !DONT ALTER!
//take copy array find a random picture
//always end with return
//copy
var tmpValue =copyArray[index];

copyArray[index] = copyArray[randIndex];
           
copyArray[randIndex] = tmpValue;
    }
return copyArray;
  
  // create a copy
}
//console.log(pictures);
console.log(getShuffledUniqueImageNames(pictures));

//Write a function productionTableWithParameters( arrayOfProducts, arrayOfPictureNames) that returns a string to
//create an HTML table. This function has to use a loop.
function productionTableWithParameters(value1){
//create table variable
var myTable ="";
//create table header row
 myTable += '<table>' ;
 myTable += '<tr><td>Product Number</td><td>Description</td><td>Available Qty</td><td>Price</td><td>Fee</td><td>Total</td><td>Image</td></tr>';
//Create loop
for (index = 0; index < value1.length; index++) {
  // continue table loop
  myTable+= '<tr>';
  myTable+= "<td>"+ value1[index].productNumber + "</td>";
  myTable+= "<td>"+ value1[index].description + "</td>";
  myTable+= "<td>"+ value1[index].availQuantity + "</td>";
  myTable+="<td>"+ value1[index].price + "</td>";
  myTable+= "<td>"+ value1[index].fee + "</td>";
  myTable+="<td>" + value1[index].totalamount + "</td>";
  myTable+= '</tr>';





}
//inside function after loop



myTable+= '</table>';
//return table variable
return myTable;


//Close function
}
// display table in html
document.getElementById('mytable').innerHTML = productionTableWithParameters(products) ;



